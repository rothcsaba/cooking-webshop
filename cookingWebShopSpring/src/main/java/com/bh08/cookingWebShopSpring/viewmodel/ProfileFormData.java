package com.bh08.cookingWebShopSpring.viewmodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProfileFormData {
	
	private String userName;
	
	private String name;
	
	private String phoneNumber;
	
	private String address;
	
	private String eMail;
	
}
