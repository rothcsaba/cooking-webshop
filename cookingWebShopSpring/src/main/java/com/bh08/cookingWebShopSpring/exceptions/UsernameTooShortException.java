package com.bh08.cookingWebShopSpring.exceptions;


public class UsernameTooShortException extends Exception {

	private static final long serialVersionUID = 1L;

	public UsernameTooShortException() {
		super("Username too short,Please enter at least 4 characters." );
	}}
	