package com.bh08.cookingWebShopSpring.exceptions;
public class UsernameIsTakenException extends Exception {
	
	
	private static final long serialVersionUID = 1L;

	public UsernameIsTakenException() {
		super("Username is already taken.");
	}
	
    
    
}