package com.bh08.cookingWebShopSpring.services;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import com.bh08.cookingWebShopSpring.models.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service

@SessionScope
public class SessionService {
	private Long userId;

	private String userName;

}
