package com.bh08.cookingWebShopSpring.services;

import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.cookingWebShopSpring.daos.UserRepository;
import com.bh08.cookingWebShopSpring.exceptions.LoginFailedException;
import com.bh08.cookingWebShopSpring.exceptions.PwDontMatchException;
import com.bh08.cookingWebShopSpring.exceptions.PwTooShortException;
import com.bh08.cookingWebShopSpring.exceptions.UsernameIsTakenException;
import com.bh08.cookingWebShopSpring.exceptions.UsernameTooShortException;
import com.bh08.cookingWebShopSpring.models.User;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User registerUser(String userName, String password, String confirmPassword)
			throws UsernameIsTakenException, UsernameTooShortException, PwDontMatchException, PwTooShortException {
		if ((userRepository.findByUserName(userName)).isPresent()) {

			throw new UsernameIsTakenException();
		}
		if (!password.equals(confirmPassword)) {
			throw new PwDontMatchException();
		}
		if (userName.length() < 4) {
			throw new UsernameTooShortException();

		}
		if (password.length() < 6) {
			throw new PwTooShortException();

		}
		String passwordHash = DigestUtils.sha1Hex(password);
		User newUser = new User(userName, passwordHash);
		userRepository.saveAndFlush(newUser);
		return newUser;
	}

	public User login(String userName, String password) throws LoginFailedException {
		if (!(userRepository.findByUserName(userName)).isPresent()) {
			throw new LoginFailedException();
		}

		String passwordHash = DigestUtils.sha1Hex(password);
		User user = (userRepository.findByUserName(userName)).get();

		if (!(user.getPasswordHash().equals(passwordHash))) {
			throw new LoginFailedException();
		}

		return user;

	}

	public User findUserByName(String userName) {

		Optional<User> user = userRepository.findByUserName(userName);

		return user.get();
	}

}