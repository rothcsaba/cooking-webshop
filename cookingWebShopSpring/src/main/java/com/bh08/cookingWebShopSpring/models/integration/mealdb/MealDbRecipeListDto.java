package com.bh08.cookingWebShopSpring.models.integration.mealdb;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MealDbRecipeListDto {
	
	private List<MealDbRecipeDto> meals;
}
