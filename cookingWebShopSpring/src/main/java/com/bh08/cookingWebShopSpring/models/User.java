package com.bh08.cookingWebShopSpring.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity implementation class for Entity: User
 *
 */

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="users")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	public User(String name, String passwordHash) {
		super();
		this.userName = name;
		this.passwordHash = passwordHash;
	}
	
	public User(Long id, String userName, String passwordHash, String name, String phoneNumber, String address,
			String eMail) {
		super();
		this.id = id;
		this.userName = userName;
		this.passwordHash = passwordHash;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.eMail = eMail;
	}

	@Id
	@GeneratedValue(generator = "userIdGenerator")
	@SequenceGenerator(name = "userIdGenerator", sequenceName = "seqUserId", initialValue = 1)
	private Long id;

	@Column(unique = true, nullable = false)
	private String userName;
	
	@Column(nullable = false)
	private String passwordHash;
	
	@Column(nullable = true)
	private String name;
	
	@Column(nullable = true)
	private String phoneNumber;
	
	@Column(nullable = true)
	private String address;
	
	@Column(nullable = true)
	private String eMail;
			
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "recipe_user", joinColumns = { @JoinColumn(name = "users_id", updatable = true, insertable = true) }, inverseJoinColumns = {
			@JoinColumn(name = "recipes_id", updatable = true, insertable = true) })
	@JsonIgnore
	private Set<Recipe> favouriteRecipes;

}