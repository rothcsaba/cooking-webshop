package com.bh08.cookingWebShopSpring.models.integration.mealdb;

import lombok.*;

@Getter
@Setter
@ToString
public class MealDbRecipeDto {

	private String idMeal;
	
}
