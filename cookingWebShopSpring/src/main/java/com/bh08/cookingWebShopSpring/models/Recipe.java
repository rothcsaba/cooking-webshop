package com.bh08.cookingWebShopSpring.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity implementation class for Entity: Recipe
 *
 */
@Entity
@Getter
@Setter
@ToString

@Table(name = "recipes")
public class Recipe implements Serializable {

	private static final long serialVersionUID = 1L;

	public Recipe() {
		super();
	}

	@Id
	// @GeneratedValue(generator = "recipeIdGenerator")
	// @SequenceGenerator(name = "recipeIdGenerator", sequenceName = "seqRecipeId",
	// initialValue = 1)
	private Long id;

	@Column(nullable = false)
	private String name;

	@Column(columnDefinition = "CLOB")
	@Lob
	private String strInstructions;

	private String mealPictureLink;

	private String mealYuotubeLink;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "recipe_product", joinColumns = { @JoinColumn(name = "recipes_id") }, inverseJoinColumns = {
			@JoinColumn(name = "products_id") })
	private Set<Product> products = new HashSet<>();

	@ManyToMany(mappedBy = "favouriteRecipes") //, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<User> users;

}