$(document).ready(function() {

	$('.increment').click(function() {

		var id = $(this).attr('id').replace('inc', '');
		var currentCount = parseInt($('#count' + id).html());
		var newCount = currentCount + 1;
		$('#count' + id).html(newCount);
		
		var pricePerUnit = parseInt($('#pricePerUnit' + id).html());
		var newSumPricePerUnit = newCount * pricePerUnit;
		$('#sumPricePerUnit' + id).html(newSumPricePerUnit);
		
		var currentSumPrice = parseInt($('#summary').html());
		var newSumPrice = currentSumPrice + pricePerUnit;
		($('#summary')).html(newSumPrice);
			
	});
	
	$('.decrease').click(function() {

		var id = $(this).attr('id').replace('dec', '');
		var currentCount = parseInt($('#count' + id).html());
		var newCount;
		if (currentCount > 0) {
			newCount = currentCount - 1;
		} else {
			newCount = currentCount;
		}
		$('#count' + id).html(newCount);
		
		var pricePerUnit = parseInt($('#pricePerUnit' + id).html());
		var newSumPricePerUnit = newCount * pricePerUnit;
		$('#sumPricePerUnit' + id).html(newSumPricePerUnit);
		
		var currentSumPrice = parseInt($('#summary').html());
		var newSumPrice = currentSumPrice + (newCount - currentCount) * pricePerUnit;
		($('#summary')).html(newSumPrice);
		
	});
});